{
    "id": "fa387955-07b1-40c6-87cc-5dc79a74ea57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4fdc3f89-b8f3-4523-bc41-93b969ed3e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa387955-07b1-40c6-87cc-5dc79a74ea57",
            "compositeImage": {
                "id": "b3c20b15-b60f-4487-a43b-321a2058d612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fdc3f89-b8f3-4523-bc41-93b969ed3e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5695e196-d815-4b61-9d91-3fa5e9df4e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fdc3f89-b8f3-4523-bc41-93b969ed3e40",
                    "LayerId": "60eadaea-053f-4797-9595-1ee1cbbe3c19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "60eadaea-053f-4797-9595-1ee1cbbe3c19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa387955-07b1-40c6-87cc-5dc79a74ea57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 64,
    "xorig": -6,
    "yorig": 35
}