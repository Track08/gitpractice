{
    "id": "f24e19b2-1ac2-45a3-857c-f352a53a3279",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "1b0e59b3-a201-41ce-9242-ec6fd80ac7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f24e19b2-1ac2-45a3-857c-f352a53a3279",
            "compositeImage": {
                "id": "16265c3d-bef4-4f7a-a239-ba44a9a84ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0e59b3-a201-41ce-9242-ec6fd80ac7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f7bb6f2-4ee9-4e0c-b446-08d31284cac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0e59b3-a201-41ce-9242-ec6fd80ac7a7",
                    "LayerId": "247ee985-9abd-40b4-863d-2cfb9341e04e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "247ee985-9abd-40b4-863d-2cfb9341e04e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f24e19b2-1ac2-45a3-857c-f352a53a3279",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 128,
    "xorig": 105,
    "yorig": 63
}