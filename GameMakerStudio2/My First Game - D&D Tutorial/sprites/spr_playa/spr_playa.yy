{
    "id": "66f7962a-2700-4744-84b7-89e4ed21befd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 113,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "cdc4c10b-4810-451f-b150-10a2d120f432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66f7962a-2700-4744-84b7-89e4ed21befd",
            "compositeImage": {
                "id": "4d5a75e3-bd92-43d5-af95-9e7b27f4f3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc4c10b-4810-451f-b150-10a2d120f432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e2997d4-5c8d-4377-ba1a-7adb7bfef04a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc4c10b-4810-451f-b150-10a2d120f432",
                    "LayerId": "d36d4253-2d3b-45aa-a46e-be7366ac28b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "d36d4253-2d3b-45aa-a46e-be7366ac28b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66f7962a-2700-4744-84b7-89e4ed21befd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroup": 0,
    "type": 0,
    "width": 114,
    "xorig": 56,
    "yorig": 57
}