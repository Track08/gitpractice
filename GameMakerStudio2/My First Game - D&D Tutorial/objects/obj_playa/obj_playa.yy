{
    "id": "a378b52f-9dee-4acd-bf34-32761cfd62ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playa",
    "eventList": [
        {
            "id": "374dc7b1-ee2e-4c10-b4b0-5beca6fcc490",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "a378b52f-9dee-4acd-bf34-32761cfd62ee"
        },
        {
            "id": "2fbfb2d5-514b-4748-ba3b-508e61a1124a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a378b52f-9dee-4acd-bf34-32761cfd62ee"
        },
        {
            "id": "2209822d-9768-4090-b491-65e3b6e24842",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "a378b52f-9dee-4acd-bf34-32761cfd62ee"
        },
        {
            "id": "72c59fbf-ffc6-4e49-a404-8fdf68ef8930",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "a378b52f-9dee-4acd-bf34-32761cfd62ee"
        },
        {
            "id": "b358be1f-4f63-4914-ad0b-3641dc1c147d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "a378b52f-9dee-4acd-bf34-32761cfd62ee"
        },
        {
            "id": "6eb42652-b8a3-4d25-8224-3c926c41073d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a378b52f-9dee-4acd-bf34-32761cfd62ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "66f7962a-2700-4744-84b7-89e4ed21befd",
    "visible": true
}